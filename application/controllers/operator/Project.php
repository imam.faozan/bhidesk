<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {


	function index()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navigationoperator');
		$this->load->view('Operator/project');
		$this->load->view('templates/footer');
	}

	function seeproject()
    {
        $this->load->view("operator/project", $data);
    }
}