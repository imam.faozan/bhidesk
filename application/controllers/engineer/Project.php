<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('Tickets_model','tickets_model');
    }


	function index()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navigationengineer');
		$this->load->view('engineer/project');
		$this->load->view('templates/footer');
	}

    function seeproject()
    {
        $this->load->view("engineer/project", $data);
    }

}