<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('Tickets_model','tickets_model');
    }


	//**** HALAMAN TICKET ****//

	function index()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navigationengineer');
		$this->load->view('engineer/tickets');
		$this->load->view('templates/footer');
	}


	function seeticket()
    {
        $this->load->view("engineer/tickets", $data);
    }


	//**** HALAMAN EDIT TICKET ****//

	function editticket($id_ticket)
	{
		$this->load->view('templates/header');
        $this->load->view('templates/navigationengineer');
        $this->load->view('engineer/editticket',array('id_ticket' => $id_ticket));
        $this->load->view('templates/footer');
	}

	//**** PROSES EDIT TICKET ****//

	function updateticket($id_ticket)
	{
			
			$id_customer= $this->input->post('id_customer');
	        $project_id= $this->input->post('project_id');
	        $email= $this->input->post('email');
	        $type= $this->input->post('type');
	        $description= $this->input->post('description');
	        $solution= $this->input->post('solution');
	        $id_user= $this->input->post('id_user');
	        $priority= $this->input->post('priority');
	        $status= $this->input->post('status');
	        $finishtime= $this->input->post('finishtime');
	        $data= array(
	        	"id_customer" => $id_customer,
	        	"project_id" => $project_id,
	        	"email" => $email,
	        	"type" => $type,
	        	"description" => $description,
	        	"solution" => $solution,
	        	"id_user" => $id_user, 
	        	"priority" => $priority,
	        	"status" => $status,
	        	"finishtime" => $finishtime);
	        $this->db->where("id_ticket",$id_ticket);
	        $this->db->update("ticket", $data);
	        
	        redirect(base_url('engineer/tickets'));
	}

}