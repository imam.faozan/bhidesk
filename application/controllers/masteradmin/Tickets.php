<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('Tickets_model','tickets_model');
    }


	//**** HALAMAN TICKET ****//

	function index()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navigationmaster');
		$this->load->view('masteradmin/tickets');
		$this->load->view('templates/footer');
	}


	//**** HALAMAN BUAT TICKET ****//

	function createticket()
	{
		$data['customer'] = $this->tickets_model->get_customer()->result();
		$data['project'] = $this->tickets_model->get_project()->result();
		$data['userlogin'] = $this->tickets_model->get_userlogin()->result();
		$this->load->view('templates/header');
		$this->load->view('templates/navigationmaster');
		$this->load->view('masteradmin/createticket',
			array(
				'customer'=>$this->tickets_model->get_customer()->result(),
				'project'=>$this->tickets_model->get_project()->result(),
				'userlogin'=>$this->tickets_model->get_userlogin()->result()));
		$this->load->view('templates/footer');
	}

	function seeticket()
    {
        $this->load->view("masteradmin/tickets", $data);
    }

	//***** PROSES BUAT TICKET *****//

	function addticket()
	{
			$id_ticket= $this->input->post('id_ticket');
			$id_customer= $this->input->post('id_customer');
	        $project_id= $this->input->post('project_id');
	        $email= $this->input->post('email');
	        $type= $this->input->post('type');
	        $description= $this->input->post('description');
	        $solution= $this->input->post('solution');
	        $id_userlogin= $this->input->post('id_userlogin');
	        $priority= $this->input->post('priority');
	        $status= $this->input->post('status');
	        $createtime=$this->input->post('createtime');
	        $data= array(
	        	"id_ticket" => $id_ticket,
	        	"id_customer" => $id_customer,
	        	"project_id" => $project_id,
	        	"email" => $email,
	        	"type" => $type,
	        	"description" => $description,
	        	"solution" => $solution,
	        	"id_userlogin" => $id_userlogin, 
	        	"priority" => $priority,
	        	"status" => $status,
	        	"createtime" => $createtime);
	        var_dump($data);
	        $this->db->insert("ticket", $data);
	        
	        redirect(base_url('masteradmin/tickets'));
	    
	}

	//**** HALAMAN EDIT TICKET ****//

	function editticket($id_ticket)
	{
		$this->load->view('templates/header');
        $this->load->view('templates/navigationmaster');
        $this->load->view('masteradmin/editticket',array('id_ticket' => $id_ticket));
        $this->load->view('templates/footer');
	}

	//**** PROSES EDIT TICKET ****//

	function updateticket($id_ticket)
	{
			
			$id_customer= $this->input->post('id_customer');
	        $project_id= $this->input->post('project_id');
	        $email= $this->input->post('email');
	        $type= $this->input->post('type');
	        $description= $this->input->post('description');
	        $solution= $this->input->post('solution');
	        $id_userlogin= $this->input->post('id_userlogin');
	        $priority= $this->input->post('priority');
	        $status= $this->input->post('status');
	        $data= array(
	        	"id_customer" => $id_customer,
	        	"project_id" => $project_id,
	        	"email" => $email,
	        	"type" => $type,
	        	"description" => $description,
	        	"solution" => $solution,
	        	"id_userlogin" => $id_userlogin, 
	        	"priority" => $priority,
	        	"status" => $status);
	        $this->db->where("id_ticket",$id_ticket);
	        $this->db->update("ticket", $data);
	        
	        redirect(base_url('masteradmin/tickets'));
	}

	//**** DELETE TICKET ***//

	function deleteticket($id_ticket)
	{
		$this->db->delete("ticket", array("id_ticket"=>$id_ticket));
    	redirect(base_url('masteradmin/tickets'));
	}

}