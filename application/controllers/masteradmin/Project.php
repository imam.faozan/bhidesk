<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('Tickets_model','tickets_model');
    }


	function index()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navigationmaster');
		$this->load->view('masteradmin/project');
		$this->load->view('templates/footer');
	}

	function createproject()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navigationmaster');
		$this->load->view('masteradmin/createproject',
			array('customer'=>$this->tickets_model->get_customer()->result()));
		$this->load->view('templates/footer');
	}

    public function editproject($project_id)
    {
        
        $this->load->view('templates/header');
        $this->load->view('templates/navigationmaster');
        $this->load->view('masteradmin/editproject',
        	array('project_id' => $project_id),
        	array('customer'=>$this->tickets_model->get_customer()->result()));
        $this->load->view('templates/footer');
    
    }

    public function seeproject()
    {
        $this->load->view("masteradmin/project", $data);
    }

    public function addproject(){
		
		$project_id= $this->input->post('project_id');
        $project_name= $this->input->post('project_name');
        $start_date= $this->input->post('start_date');
        $end_date= $this->input->post('end_date');
		$id_customer= $this->input->post('id_customer');
        $data=array(
			"project_id" => $project_id,
        	"project_name" => $project_name,
        	"start_date" => $start_date, 
        	"end_date" => $end_date,
			"id_customer" => $id_customer);
        $this->db->insert("project", $data);
        
        redirect(base_url('masteradmin/project'));
        
    }

    public function updateproject($project_id){
        $project_name= $this->input->post('project_name');
        $start_date= $this->input->post('start_date');
        $end_date= $this->input->post('end_date');
		$id_customer= $this->input->post('id_customer');
        $data=array(
            "project_id" => $project_id,
            "project_name" => $project_name,
            "start_date" => $start_date, 
            "end_date" => $end_date,
			"id_customer" => $id_customer
		);
        $this->db->where("project_id",$project_id);
        $this->db->update("project", $data);
        
        redirect(base_url('masteradmin/project'));
        
    }

    public function deleteproject($project_id)
    {
    $this->db->delete("project", array("project_id"=>$project_id));
    redirect(base_url('masteradmin/project'));
}

   
	function detailproject()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navigationmaster');
		$this->load->view('masteradmin/detailproject');
		$this->load->view('templates/footer');
	}
}