<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {


	function index()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navigationmaster');
		$this->load->view('masteradmin/customer');
		$this->load->view('templates/footer');
	}

	function createcustomer()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navigationmaster');
		$this->load->view('masteradmin/createcustomer');
		$this->load->view('templates/footer');
	}

    function editcustomer($id_customer)
    {
        
        $this->load->view('templates/header');
        $this->load->view('templates/navigationmaster');
        $this->load->view('masteradmin/editcustomer',array('id_customer' => $id_customer));
        $this->load->view('templates/footer');
    
    }

    public function seecustomer()
    {
        $this->load->view("masteradmin/customer", $data);
    }

    public function addcustomer(){
        
        $customer_name= $this->input->post('customer_name');
        $customer_address= $this->input->post('customer_address');
        $customer_contact= $this->input->post('customer_contact');
        $customer_email= $this->input->post('customer_email');
        $data=array(
        	"customer_name" => $customer_name,
        	"customer_address" => $customer_address,
        	"customer_contact" => $customer_contact, 
        	"customer_email" => $customer_email);
        $this->db->insert("customer", $data);
        
        redirect(base_url('masteradmin/customer'));
        
    }

    public function updatecustomer($id_customer){
        
        $customer_name= $this->input->post('customer_name');
        $customer_address= $this->input->post('customer_address');
        $customer_contact= $this->input->post('customer_contact');
        $customer_email= $this->input->post('customer_email');
        $data=array(
            "customer_name" => $customer_name,
            "customer_address" => $customer_address,
            "customer_contact" => $customer_contact, 
            "customer_email" => $customer_email);
        $this->db->where ("id_customer",$id_customer);
        $this->db->update("customer", $data);
        
        redirect(base_url('masteradmin/customer'));
        
    }

    public function deletecustomer($id_customer)
    {
    $this->db->delete("customer", array("id_customer"=>$id_customer));
    redirect(base_url('masteradmin/customer'));
}

   

	function detailcustomer()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navigationmaster');
		$this->load->view('masteradmin/detailcustomer');
		$this->load->view('templates/footer');
	}
}