<?php date_default_timezone_set("Asia/Jakarta"); ?>

<!-- Content Wrapper. Contains page content --> 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create New Ticket 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tickets</li>
        <li class="active">Create Ticket</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- /.row -->
     <div class="box box-danger">
            <div class="box-header with-border">
            </div>
            <div class="box-body">
              <form action="<?php echo base_url(). '/operator/tickets/addticket'; ?>" method="post">
                <table style="margin:20px auto;">


                 <!--pilih customer -->
                <div class="form-group">
                  <label>Customer</label>
                  <select class="form-control" id="id_customer" name="id_customer">
                    <option value>Choose Customer</option>
                    <?php foreach ($customer as $cst) : ?>
                        <option value="<?php echo $cst->id_customer; ?>"><?php echo $cst->customer_name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <!-- pilih project -->
                <div class="form-group">
                  <label>Project</label>
                  <select class="form-control" id="project_id" name="project_id">
                    <option value>Choose Project</option>
                    <?php foreach ($project as $prj) : ?>
                        <option value="<?php echo $prj->project_id; ?>"><?php echo $prj->project_name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <!-- customer email -->
                <div class="form-group">
                  <label>Cust. Email</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" class="form-control" name="email" placeholder="Email">
                  </div>
                </div>

                <!-- pilih tipe -->
                <div class="form-group">
                  <label>Type</label>
                  <select class="form-control" id="type" name="type">
                    <option value="">Choose Ticket Type</option>
                    <option value="service request">Service Request</option>
                    <option value="problem issue">Problem Issue</option>
                  </select>
                </div>

                <!-- Deskripsi masalah -->
                <div class="form-group">
                  <label>Description</label>
                  <textarea name ="description" class="form-control" rows="3" placeholder="Desc Request / Problem"></textarea>
                </div>

                <!-- Solusi -->
                <div class="form-group">
                  <label>Solution</label>
                  <textarea name="solution" class="form-control" rows="3" placeholder="Solution and Action"></textarea>
                </div>

                <!-- pilih Engineer -->
                <div class="form-group">
                  <label>Assign to</label>
                  <select class="form-control" id="id_user" name="id_user">
                    <option value>Choose Engineer</option>
                    <?php foreach ($user as $eng) : ?>
                        <option value="<?php echo $eng->id_user; ?>"><?php echo $eng->userlogin_alias; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <!-- Priority -->
                <div class="form-group">
                  <label>Priority</label>
                  <select class="form-control" id="priority" name="priority">
                    <option value="">Choose Priority</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select>
                </div>

                <!--Status-->
                <div class="form-group">
                  <label>Status</label>
                  <input type="text" class="form-control" name="status" value="OPEN" readonly>
                  </select>
                </div>

                <!--Timestamp-->

                <div class="hidden">
                  <label>Create Time</label>
                  <input value="<?php echo date("Y-m-d H:i:s"); ?>" class="form-control date" name="createtime" readonly></div>


                <!-- Submit -->
                <div class="form-group">
                  <button type="submit" class="btn btn-info">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
