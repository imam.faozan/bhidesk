<!-- Content Wrapper. Contains page content --> 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detail Customer 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Customerss</li>
        <li class="active">Detail Customer</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- /.row -->
     <div class="box box-danger">
            <div class="box-header with-border">
            </div>
            <div class="box-body">
              <form role="form">

                <!-- customer name -->
                <div class="form-group">
                  <label>Customer</label>
                 <input type="text" class="form-control" placeholder="Siloam Hospitals International" disabled>
                </div></form>

                 <!--List Project -->
                <div class="form-group">
                  <label>List Project</label>
                  <textarea class="form-control" rows="5" placeholder=" Project 1"></textarea>
                </div>

                <!-- Submit -->
                <div class="form-group">
                  <button type="submit" class="btn btn-info">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
