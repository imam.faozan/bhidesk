<!-- Content Wrapper. Contains page content --> 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customer List  
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Customer</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">
              </h3>
              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Customer Name</th>
                  <th>Address</th>
                  <th>Contact</th>
                  <th>Email</th>
                    
                </tr>
                <tr>

                  <tbody>
                    <?php
                    $i=0;
                    $GET_DATA = $this->db->get_where('customer');
                    foreach($GET_DATA->result() as $Result):
                      $i++;
                ?>
                  
                  <tr>
                    <td ><?php echo $i;?> </td>
                    <td>
                      <?php echo $Result->customer_name; ?>
                    </td>
                    <td>  
                      <?php echo $Result->customer_address; ?>
                    </td>
                    <td>
                      <?php echo $Result->customer_contact; ?>
                    </td>
                    <td>
                      <?php echo $Result->customer_email; ?>
                    </td>
                      
                
                  </tr>
                  <?php endforeach; ?>

                </tbody>
                 
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  