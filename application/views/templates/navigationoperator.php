 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo base_url('operator/welcome');?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('operator/tickets');?>">
            <i class="fa fa-ticket"></i>
            <span>Tickets</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('operator/customer');?>">
            <i class="fa fa-users"></i> <span>Customer</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('operator/project');?>">
            <i class="fa fa-laptop"></i>
            <span>Project</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Reports</span>
            <span class="pull-right-container">
            </span>
          </a>
          
    </section>
    <!-- /.sidebar -->
  </aside>