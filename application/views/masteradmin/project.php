<!-- Content Wrapper. Contains page content --> 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Project List  
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Project</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                  <a href="<?php echo site_url('masteradmin/project/createproject/editproject');?>"><span class="btn btn-large btn-danger">Create</span></a>
              </h3>
              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Project ID</th>
                  <th>Project Name</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Customer</th>
                  <th>Action</th>
                    
                </tr>
                <tr>

                  <tbody>
                    <?php
                    $GET_DATA = $this->db->query('SELECT * FROM project a inner join customer b on b.id_customer = a.id_customer ORDER by project_id ASC');
                    foreach($GET_DATA->result() as $Result):
                ?>
                  
                    <td><?php echo $Result->project_id; ?>
                    </td>
                    <td>  
                      <?php echo $Result->project_name; ?>
                    </td>
                    <td>
                      <?php echo $Result->start_date; ?>
                    </td>
                    <td>
                      <?php echo $Result->end_date; ?>
                    </td>
                    <td>
                      <?php echo $Result->customer_name; ?>
                    </td>
                    <td width="200">
                      <a href="<?php echo base_url('masteradmin/project/editproject/'.$Result->project_id); ?>" 
                        class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-edit"></span></a> ||
                      <a href="<?php echo base_url('masteradmin/project/deleteproject/'.$Result->project_id); ?>" 
                        class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                      
                
                  </tr>
                  <?php endforeach; ?>

                </tbody>
                 
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  

  