<!-- Content Wrapper. Contains page content --> 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tickets List  
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tickets</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                  <a href="<?php echo site_url('masteradmin/tickets/createticket/edittickets');?>"><span class="btn btn-large btn-danger">Create</span></a>
              </h3>
              <div class="box-tools">
                <!-- Date and time range -->
              <div class="form-group">
                <div class="input-group input-group input-group-sm hidden-xs">
                  <button type="button" class="btn btn-default pull-left" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> 
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                </div>
              </div>
            </div>
              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  
                  <th>Ticket ID</th>
                  <th>Type</th>
                  <th>Description</th>
                  <th>Assign to</th>
                  <th>Project</th>
                  <th>Pri</th>
                  <th>Status</th>
                  <th>Created</th>
                  <th>Aksi</th>
                </tr>

                <tr>
                 <tbody>
                    <tbody>
                    <?php
                    $GET_DATA = $this->db->query('SELECT * FROM ticket a inner join project b on b.project_id = a.project_id inner join userlogin c on c.id_userlogin = a.id_userlogin ORDER by id_ticket ASC');
                    foreach($GET_DATA->result() as $Result):
                ?>
                
                <tr>

                    <td>
                      <a href="<?php echo site_url('masteradmin/tickets/editticket/'.$Result->id_ticket); ?>">
                        <?php echo "BSITICKET00000".$Result->id_ticket; ?>
                    </td>

                    <td>  
                      <?php echo $Result->type; ?>
                    </td>

                    <td>
                      <?php echo $Result->description; ?>
                    </td>

                    <td>
                      <?php echo $Result->fullname  ; ?>
                      </td>

                    <td>
                      <?php echo $Result->project_name; ?>
                      </td>

                    <td>
                      <?php echo $Result->priority; ?>
                    </td>

                    <td>
                      <?php echo $Result->status; ?></td>

                      <td>
                      <?php echo $Result->createtime; ?>
                    </td>

                    <td>
                      <a href="<?php echo base_url('masteradmin/tickets/editticket/'.$Result->id_ticket); ?>" 
                        class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-edit"></span></a> ||
                      <a href="<?php echo base_url('masteradmin/tickets/deleteticket/'.$Result->id_ticket); ?>" 
                        class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                      </td>

                    </tr>
                    <?php endforeach; ?>
                    
                  <td></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  

  