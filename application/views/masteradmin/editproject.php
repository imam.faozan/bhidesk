<!-- Content Wrapper. Contains page content --> 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Project
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Project</li>
        <li class="active">Edit Project</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- /.row -->
     <div class="box box-danger">
            <div class="box-header with-border">
            </div>
            <div class="box-body"> 
              <?php
                  $query=$this->db->get_where("project",array('project_id' =>$project_id));
                  foreach ($query->result()as $dataproject);
                    ?>
                <form action="<?php echo base_url(). 'masteradmin/project/updateproject/'.$project_id; ?>" method="post">
                <table style="margin:20px auto;">
                
				<!-- Project ID-->
                <div class="form-group">
                  <label>Project ID</label>
                <input type ="text" class="form-control" placeholder="ID Project" name="project_id" value="<?php echo $dataproject->project_id; ?>">
                </div>

                <!-- Project Name -->
                <div class="form-group">
                  <label>Project Name</label>
                  <textarea name ="project_name" class="form-control" rows="2" placeholder="project name" name="project_name"><?php echo $dataproject->project_name; ?></textarea>
                </div>

                <!-- Customer -->
                <div class="form-group">
                  <label>Customer</label> 
                  <select class="form-control" id="id_customer" name="id_customer">
                    <?php 
                    $customer = $this->db->get_where('customer')->result();
                    foreach ($customer as $cst) : ?>
                        <option value="<?php echo $cst->id_customer; ?>" <?php if ($dataproject->id_customer == $cst->id_customer){ echo "selected";} ?>>
                          <?php echo $cst->customer_name; ?></option>
                    <?php endforeach; ?>
                  </select></div>

                <!-- Start Date -->
                <div class="form-group">
                  <label>Start Date</label>
				  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input name ="start_date" type="date" class="form-control" placeholder="start date" name="start_date" value="<?php echo $dataproject->start_date; ?>">
                  </div>


                <!-- End Date -->
                <div class="form-group">
                  <label>End Date</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input name ="end_date" type="date" class="form-control" placeholder="end date" name="end_date" value="<?php echo $dataproject->end_date; ?>">
                  </div>

				  </div>			
                </div>
                <!-- Submit -->
                <div class="form-group">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="cancel" class="btn btn-info">Cancel</button>
                 
                </div>
              </form>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
