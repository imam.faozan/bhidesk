<!-- Content Wrapper. Contains page content --> 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Customer 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Customers</li>
        <li class="active">Edit Customer</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- /.row -->
     <div class="box box-danger">
            <div class="box-header with-border">
            </div>
            <div class="box-body"> 
              <?php
                  $query=$this->db->get_where("customer",array('id_customer' =>$id_customer));
                  foreach ($query->result()as $dataCustomer);
                    ?>
                <form action="<?php echo base_url(). 'masteradmin/customer/updatecustomer/'.$id_customer; ?>" method="post">
                <table style="margin:20px auto;">
                
              
                <!-- Customer Name-->
                <div class="form-group">
                  <label>Customer Name</label>
                 <input type="text" class="form-control" placeholder="Customer Name" name="customer_name" value="<?php echo $dataCustomer->customer_name; ?>">
                </div>

                <!-- Customer Address -->
                <div class="form-group">
                  <label>Customer Address</label>
                  <textarea class="form-control" rows="2" placeholder="Address" name="customer_address"><?php echo $dataCustomer->customer_address; ?></textarea>
                </div>

                <!-- customer contact -->
                <div class="form-group">
                  <label>Contact</label>
                 <input type="text" class="form-control" placeholder="No Telp" name="customer_contact" value="<?php echo $dataCustomer->customer_contact; ?>">
                </div>

                <!-- customer email -->
                <div class="form-group">
                  <label>Cust. Email</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="email" class="form-control" placeholder="Email" name="customer_email" value="<?php echo $dataCustomer->customer_email; ?>">
                  </div>
                </div>

                <!-- Submit -->
                <div class="form-group">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="cancel" class="btn btn-info">Cancel</button>
                 
                </div>
              </form>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
