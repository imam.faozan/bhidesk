<!-- Content Wrapper. Contains page content --> 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <?php
            $query=$this->db->get_where("ticket",array('id_ticket' =>$id_ticket));
            foreach ($query->result()as $dataticket); ?>
      <h1><?php echo "BSITICKET00000".$dataticket->id_ticket; ?> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tickets</li>
        <li class="active">Edit <?php echo "BSITICKET00000".$dataticket->id_ticket; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- /.row -->
     <div class="box box-danger">
            <div class="box-header with-border">
            </div>
            <div class="box-body">
               <form action="<?php echo base_url(). 'masteradmin/tickets/updateticket/'.$id_ticket; ?>" method="post">
                <table style="margin:20px auto;">

                <!--Time Created-->
                <div class="form-group">
                  <label>Ticket Created</label>
                  <input type="text" class="form-control" value="<?php echo $dataticket->createtime; ?>" readonly></div>

                 <!--pilih customer -->
                <div class="form-group">
                  <label>Customer</label> 
                  <select class="form-control" id="id_customer" name="id_customer">
                    <?php 
                    $customer = $this->db->get_where('customer')->result();
                    foreach ($customer as $cst) : ?>
                        <option value="<?php echo $cst->id_customer; ?>" <?php if ($dataticket->id_customer == $cst->id_customer)?>>
                          <?php echo $cst->customer_name; ?></option>
                    <?php endforeach; ?>
                  </select></div>

                <!-- pilih project -->
                <div class="form-group">
                  <label>Project</label>
                  <select class="form-control" id="project_id" name="project_id">
                    <?php
                    $project = $this->db->get_where('project')->result();
                    foreach ($project as $prj) : ?>
                        <option value="<?php echo $prj->project_id; ?>"><?php if ($dataticket->project_id == $prj->project_id) ?>
                          <?php echo $prj->project_name; ?>
                        </option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <!-- customer email -->
                <div class="form-group">
                  <label>Cust. Email</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $dataticket->email; ?>">
                  </div>
                </div>

                <!-- pilih tipe -->
                <div class="form-group">
                  <label>Type</label>
                  <select class="form-control" id="type" name="type">
                    <option value="<?php echo $dataticket->type; ?>"><?php echo $dataticket->type; ?></option>
                    <option value="service request">Service Request</option>
                    <option value="problem issue">Problem Issue</option>
                  </select>
                </div>

                <!-- Deskripsi masalah -->
                <div class="form-group">
                  <label>Description</label>
                  <textarea name ="description" class="form-control" rows="3" placeholder="Desc Request / Problem"><?php echo $dataticket->description; ?></textarea>
                </div>

                <!-- Solusi -->
                <div class="form-group">
                  <label>Solution</label>
                  <textarea name="solution" class="form-control" rows="3" placeholder="Solution and Action"><?php echo $dataticket->solution; ?></textarea>
                </div>

                <!-- pilih Engineer -->
                <div class="form-group">
                  <label>Assign to</label>
                  <select class="form-control" id="id_userlogin" name="id_userlogin">
                    <?php
                    $user = $this->db->get_where('userlogin')->result();
                    foreach ($user as $usr) : ?>
                        <option value="<?php echo $usr->id_userlogin; ?>"><?php if ($dataticket->id_userlogin == $usr->id_userlogin) ?>
                          <?php echo $usr->fullname; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <!-- Priority -->
                <div class="form-group">
                  <label>Priority</label>
                  <select class="form-control" id="priority" name="priority">
                    <option value="<?php echo $dataticket->priority; ?>"><?php echo $dataticket->priority; ?></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select>
                </div>

                <!--Status-->
                <div class="form-group">
                  <label>Status</label>
                  <select class="form-control" id="status" name="status">
                    <option value=""><?php echo $dataticket->status; ?></option>
                    <option value="Open">OPEN</option>
                    <option value="On Progress">ON PROGRESS</option>
                    <option value="Finish">FINISH</option>
                    <option value="Closed">CLOSED</option>
                  </select>
                </div>


                <!-- Submit -->
                <div class="form-group">
                  <button type="submit" class="btn btn-info">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
