<!-- Content Wrapper. Contains page content --> 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create New Project
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Project</li>
        <li class="active">Create Project</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- /.row -->
     <div class="box box-danger">
            <div class="box-header with-border">
            </div>
            <div class="box-body">
            <form action="<?php echo base_url(). 'masteradmin/project/addproject'; ?>" method="post">
                <table style="margin:20px auto;">
                

                 <!-- Project ID-->
                <div class="form-group">
                  <label>Project ID</label>
                 <input type ="text" class="form-control" placeholder="ID Project" name="project_id">
                </div>  

                <!-- Project Name -->
                <div class="form-group">
                  <label>Project Name</label>
                  <textarea name ="project_name" class="form-control" rows="2" placeholder="Project Name"></textarea>
                </div>


               <!--pilih customer -->
                <div class="form-group">
                  <label>Customer</label> 
                  <select class="form-control" id="id_customer" name="id_customer">
                    <option value>Choose Customer</option>
                    <?php foreach ($customer as $cst) : ?>
                        <option value="<?php echo $cst->id_customer; ?>"><?php echo $cst->customer_name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <!-- Start Date -->
                <div class="form-group">
                  <label>Start Date</label>
				  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input name ="start_date" type="date" class="form-control" placeholder="Project Start" style="width: 155px">
                  </div></div>


                <!-- End Date -->
                <div class="form-group">
                  <label>End Date</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input name ="end_date" type="date" class="form-control" placeholder="Project End" style="width: 155px" >
                  </div></div>

                <!-- Submit -->
                <div class="form-group">
                  <button type="submit" class="btn btn-info">Submit</button>
				          <button type="cancel" class="btn btn-danger">Cancel</button>
				  
                </div>
              </form>
			  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
