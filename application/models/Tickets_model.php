<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Tickets_model extends CI_Model{
     
    function get_customer(){
        $query = $this->db->get('customer');
        return $query;  
    }

    function get_project(){
    	$query = $this->db->get('project');
    	return $query;
         }

     function get_userlogin(){
    	$query = $this->db->get('userlogin');
    	return $query;
         }}
